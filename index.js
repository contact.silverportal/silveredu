const express = require('express');
const http = require('http');
const https = require('https');
const fs = require('fs');
var path = require('path');
const httpPort = 3000;
const httpsPort = 3001;
app = express()

var key = fs.readFileSync(__dirname + '/certsFiles/selfsigned.key');
var cert = fs.readFileSync(__dirname + '/certsFiles/selfsigned.crt');

var credentials = {
  key: key,
  cert: cert
};

app.use(express.static('public'));
//GET home route
app.get(['/','/*'], (req, res) => {
   res.sendFile(path.join(__dirname, '/public', 'index.html'));
});

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);


httpServer.listen(httpPort, () => {
  console.log("Http server listing on port : " + httpPort)
});

httpsServer.listen(httpsPort, () => {
  console.log("Https server listing on port : " + httpsPort)
});
